import { Link } from "react-router-dom"

const NotFound = () => {
    return ( 
        <div className="not-found">
            <h2>Sorry</h2>
            <p>Sorry wrong url</p>
            <Link to="/">To Home</Link>
        </div>
     );
}
 
export default NotFound;
import BlogList from "./blog-list";
import useFetch from "./useFetch";

const Home = () => {
    const {data, ErrMsg, loading}= useFetch('http://localhost:3002/blogs');

    
    return ( 
        <div className="home">
            {ErrMsg && <div>{ErrMsg}</div>}
            {loading && <div>Loading...</div>}
            {data && <BlogList blogs={data} title="All Blogs" />}
        </div>
     );
}
 
export default Home;

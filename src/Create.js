import { useState } from "react";
import { useHistory } from "react-router";

const Create = () => {
    const [title, setTitle]= useState ('');
    const [body, setBody]= useState ('');
    const [author, setAuthor]= useState ('mario');
    const [loading, setLoading]= useState (false);
    const history=useHistory();

    const handleSubmit= (e)=> {
        e.preventDefault();
        const blog = {title, body, author};
        
        setLoading(true);

        fetch('http://localhost:3002/blogs', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(blog)
        }).then(()=>{
            console.log('New Blog Added')
            setLoading(false);
            // history.go(-1);
            history.push('/')
        })

    }

    return ( 
        <div className="create">
            <h1><center>Create a New Blog</center></h1>
            <div className="create-form">
                <form onSubmit={handleSubmit} className="c-form">
                    <p>Title: </p>
                    <input type="text" required onChange={(e)=>setTitle(e.target.value)} name="title" id="" placeholder='Enter Title'/>
                    <p>Body:</p>
                    <textarea name="body" id="" onChange={(e)=>setBody(e.target.value)} required cols="30" rows="10" placeholder="Write Here"></textarea>
                    <p>Author: </p>
                    <select value={author} onChange={(e)=>setAuthor(e.target.value)}>
                        <option value="mario">mario</option>
                        <option value="yoshi">yoshi</option>
                    </select>
                    { !loading && <button>Add Blog</button>}
                    { loading && <button disabled>Adding Blog</button>}
                </form>
            </div>
        </div>
     );
}
 
export default Create;
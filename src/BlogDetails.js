import { useHistory, useParams } from "react-router";
import useFetch from "./useFetch";

const BlogDetails = () => {
    const { id } = useParams();
    const {data, error, loading} = useFetch("http://localhost:3002/blogs/"+ id)
    const history=useHistory();

    const handleClick = () => {
        fetch('http://localhost:3002/blogs/'+ data.id, {
            method: 'DELETE'
        })
        .then(()=>{
            history.push('/')
        })
    }

    return ( 
        <div className="blog-details">
            {loading && <div>Loading....</div>}
            {error && <div>{ error }</div>}
            {data && (
                <article>
                    <h2>{ data.title }</h2>
                    <p>Written By: { data.author }</p>
                    <div>{ data.body }</div>
                    <button onClick={handleClick}>Delete</button>
                </article>
            )}
        </div>
     );
}
 
export default BlogDetails;
import { useEffect, useState } from "react";

const useFetch = (url) => {
    const [data, setData] = useState(null);
    const [loading, setLoading] = useState(true);
    const [ErrMsg, setErrMsg] = useState(null);

    useEffect(()=>{
            const abortCont = new AbortController();
            fetch(url, {signal: abortCont.signal})
                .then(res=> {
                    if (!res.ok) {
                        throw Error ('Incorrect resource')
                    }
                    return res.json();
                    })
                .then(data=> {
                    setData(data);
                    setLoading(false);
                })
                .catch(err => {
                    if (err.name==='AbortError') {
                        console.log('fetch aborted')
                    }
                    else {
                    setErrMsg(err.message);
                    setLoading(false);
                    }
                });

                return ()=> abortCont.abort();
    }, [url])
    return {data, loading, ErrMsg};
}
 
export default useFetch;
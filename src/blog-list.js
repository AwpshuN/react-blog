import { Link } from "react-router-dom";

const BlogList = ({ blogs, title, handleDelete }) => {

    return (
        <div className="blog-list">
            <h1>{title}</h1>
            {
                blogs.map((blog) => (
                    <Link to={`/blogs/${blog.id}`} key={blog.id}>
                        <div className="blog-prev" >
                            <h2>{blog.title}</h2>
                            <div className="blog-con">
                                <p>Author: {blog.author}</p>
                            </div>
                        </div>
                    </Link>
                ))}
        </div>
    );
}

export default BlogList;